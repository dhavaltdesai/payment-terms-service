package com.immobel.paymenttermsservice.application.controller;


import com.immobel.paymenttermsservice.application.PaymentTermAppService;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.immobel.paymenttermsservice.domain.model.PaymentTerm;

import java.util.Optional;

@RestController
public class PTSController {

    @Autowired
    private PaymentTermAppService PTAppService;

    @PostMapping("/api/paymentterm")
    public ResponseEntity<PaymentTerm> createPaymentTerm(@RequestBody PaymentTerm paymentTerm)
    {
        PaymentTerm createdPT = PTAppService.createNewPaymentTerm(paymentTerm);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(createdPT);



    }

    @GetMapping("/api/paymentterm/{ptcode}")
    public ResponseEntity<PaymentTerm> getPaymentTermById(@PathVariable("ptcode") Integer PTCode)
    {
        Optional<PaymentTerm> fromDB = PTAppService.findById(PTCode);
        if(fromDB.isPresent())
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fromDB.get());
        }
        else
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .build();

        }



    }

    @PutMapping("/api/paymentterm")
    public ResponseEntity<PaymentTerm> updatePaymentTerm(@RequestBody PaymentTerm paymentTerm)
    {

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(PTAppService.update(paymentTerm));
    }

    @DeleteMapping("/api/paymentterm/{id}")
    public ResponseEntity deletePaymentTermByCode(@PathVariable("id") Integer PTId)
    {
        PTAppService.deletePaymentTerm(PTId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }

    @GetMapping
    public HttpStatus readynessCheck()
    {
        return HttpStatus.OK;
    }

    @GetMapping("/api/paymenttermcode")
    public ResponseEntity<PaymentTerm> getPaymentTermByCode(@RequestParam("termcode") String termCode )
    {
        Optional<PaymentTerm> fromDB = PTAppService.findByPaymentTermCode(termCode);

        if(fromDB.isPresent())
        {

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fromDB.get());
        }
        else
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .build();

        }

    }
}