package com.immobel.paymenttermsservice.application;

import com.immobel.paymenttermsservice.domain.model.PaymentTerm;
import com.immobel.paymenttermsservice.infrastructure.repository.PaymentTermRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PaymentTermAppService {

    @Autowired
    private PaymentTermRepo PTRepo;

    public PaymentTerm createNewPaymentTerm(PaymentTerm pt)
    {
        return PTRepo.save(pt);
    }

    public Optional<PaymentTerm> findById(Integer PTId)
    {
        Optional<PaymentTerm> ptFound = PTRepo.findById(PTId);
        return ptFound.isPresent()?ptFound:Optional.empty();

    }

    public Optional<PaymentTerm> findByPaymentTermCode(String PTCode)
    {
        Optional<PaymentTerm> ptFound = PTRepo.findByPaymentTermCode(PTCode);
        return ptFound.isPresent()?ptFound:Optional.empty();

    }

    public PaymentTerm update(PaymentTerm PT)
    {
        return PTRepo.save(PT);
    }

    //TODO : replace void by some kind of confirmation back to user
    public void deletePaymentTerm(Integer PTId)
    {
        PTRepo.deleteById(PTId);
    }


}
