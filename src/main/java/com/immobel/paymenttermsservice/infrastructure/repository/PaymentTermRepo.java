package com.immobel.paymenttermsservice.infrastructure.repository;

import com.immobel.paymenttermsservice.domain.model.PaymentTerm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PaymentTermRepo extends CrudRepository<PaymentTerm, Integer>
{
    //TODO return list for the possibility of finding more than one
    public Optional<PaymentTerm> findByPaymentTermCode(String paymentTermCode);

}
